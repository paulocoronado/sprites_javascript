

function cambiarMario(){

    //Crear un objeto que haga referencia al elemento HTML cuyo ID es spriteMario
    const mario=document.querySelector('#spriteMario');

    //Crear un objeto con todos los estilos que tiene el objeto
    const estilosMario=window.getComputedStyle(mario)

    //crear una variable para guardar la cadena de texto de la propiedad background-position

    const estiloFondo = estilosMario.getPropertyValue("background-position");

    console.log(estiloFondo);

    //separar las dos cadenas de texto que corresponde a las posiciones X,Y
    const coordenadas=estiloFondo.split(" ");

    let x= coordenadas[0];
    let y= coordenadas[1];

    //remover las letras px
    x=x.substring(0, x.length-2);
    y=y.substring(0, y.length-2);

    //Convertir a número

    x= Number.parseInt(x);
    y= Number.parseInt(y);

    //Calcular las nuevas coordenadas del fondo
    if(x==-200){
        x=-100;
    }else{
        x= x-50;
    }

    //Mofiicar el estilo "background-position" del objeto
    mario.style.backgroundPosition=x+ "px "+y+ "px";






}